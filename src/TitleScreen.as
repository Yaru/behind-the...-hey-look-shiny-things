package  
{
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import org.flixel.plugin.FlxTextPlus;
	/**
	 * ...
	 * @author Yaru
	 */
	public class TitleScreen extends FlxState
	{
		
		override public function create():void 
		{
			var t:FlxTextPlus = new FlxTextPlus(90, 60, 200);
			t.text = "MOVE WITH A, D, AND SPACE \n GRAB ALL JEWELS TO WIN \n IF YOU PRESS ENTER, YOU'LL RETURN TO THE SURFACE \n\n\n\n PRESS ENTER TO START"
			add(t);
			super.create();
		}
		
		override public function update():void 
		{
			if (FlxG.keys.justPressed("ENTER")) {
				FlxG.switchState(new PlayState);
			}
			super.update();
		}
	}

}