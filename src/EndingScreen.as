package  
{
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import org.flixel.FlxU;
	import org.flixel.plugin.FlxTextPlus;
	
	/**
	 * ...
	 * @author Yaru
	 */
	public class EndingScreen extends FlxState 
	{
		
		override public function create():void 
		{
			var t:FlxTextPlus = new FlxTextPlus(130, 50, 200);
			t.text = "THANKS FOR PLAYING! \n YOU TOOK " + FlxU.formatTime(Util.timer).toString() + "\n PLAY AGAIN TO IMPROVE YOUR TIME!\nPRESS ENTER TO RESTART"
			add(t);
			super.create();
		}
		
		override public function update():void 
		{
			if (FlxG.keys.justPressed("ENTER")) {
				FlxG.resetGame();
			}
			super.update();
		}
		
	}

}