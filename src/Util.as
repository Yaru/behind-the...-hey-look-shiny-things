package 
{
	import flash.display.ShaderParameter;
	import flash.display.Shape;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;
	import org.flixel.FlxText;
	/**
	 * ...
	 * @author Yaru
	 */
	public class Util 
	{
		public static const RED:uint = 0xFFFF0000;
		public static const GREEN:uint = 0xFF00FF00;
		public static const BLUE:uint = 0xFF0000FF;
		public static const BLACK:uint = 0xFF000000;
		public static const WHITE:uint = 0xFFFFFFFF;
		public static const CYAN:uint = 0xFF00FFFF;
		public static const MAGENTA:uint = 0xFFFF00FF;
		public static const YELLOW:uint = 0xFFFFFF00;
		public static const LIGHT_GREY:uint = 0xFFCCCCCC;
		public static const MID_GREY:uint = 0xFF999999;
		public static const DARK_GREY:uint = 0xFF666666;
		
		public static var timer:Number = 0;
		
		public static function randomNumber(min:Number, max:Number):Number
		{
			return Math.floor(Math.random() * (1 + max - min) + min);
		}
		
		
			
	}
}