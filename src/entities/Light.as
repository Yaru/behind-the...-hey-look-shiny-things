package entities 
{
	import org.flixel.*;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Yaru
	 */
	public class Light extends FlxSprite
	{
		[Embed(source = "../../assets/images/light.png")] private var LightImage:Class;
		
		private var darkness:FlxSprite;
		
		public function Light(X:Number, Y:Number, darkness:FlxSprite, color:Number, scaleX:Number = 1,scaleY:Number = 1) 
		{
			super(X, Y, LightImage);
			this.darkness = darkness;
			this.blend = "screen";
			this.color = color;
			this.scale.x = scaleX;
			this.scale.y = scaleY;
			
		}
		
	}

}