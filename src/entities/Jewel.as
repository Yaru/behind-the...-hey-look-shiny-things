package entities 
{
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Yaru
	 */
	public class Jewel extends FlxSprite
	{
		
		public function Jewel(X:Number,Y:Number) 
		{
			super(X, Y);
			makeGraphic(8, 8, 0xFF00FFFF);
		}
		
	}

}