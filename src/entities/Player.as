package entities 
{
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	import org.flixel.plugin.photonstorm.FlxControl;
	import org.flixel.plugin.photonstorm.FlxControlHandler;
	
	/**
	 * ...
	 * @author Yaru
	 */
	public class Player extends FlxSprite 
	{
		private var xStart:Number;
		private var yStart:Number;
		public function Player(X:Number=0, Y:Number=0) 
		{
			super(X, Y);
			xStart = X;
			yStart = Y;
			makeGraphic(8, 8, 0xFFFF0000);
			if (FlxG.getPlugin(FlxControl) == null) {
				FlxG.addPlugin(new FlxControl());
			}
			FlxControl.create(this, FlxControlHandler.MOVEMENT_ACCELERATES, FlxControlHandler.STOPPING_DECELERATES, 1, true, false);
			FlxControl.player1.setWASDControl(false, false, true, true);
			FlxControl.player1.setJumpButton("SPACE", FlxControlHandler.KEYMODE_PRESSED, 200, FlxObject.FLOOR, 250, 200);
			FlxControl.player1.setMovementSpeed(400, 0, 100, 200, 400, 0);
			FlxControl.player1.setGravity(0, 400);
		}
		
		public function returnToStart():void
		{
			FlxG.flash(Util.WHITE, 0.2);
			x = xStart;
			y = yStart;
		}
		
	}

}