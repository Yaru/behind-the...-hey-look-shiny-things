package  
{
	import entities.Jewel;
	import entities.Light;
	import entities.Player;
	import flash.events.*;
	import flash.net.*;
	import net.pixelpracht.tmx.*;
	import org.flixel.*;
	import org.flixel.plugin.FlxTextPlus;
	
	
	/**
	 * ...
	 * @author Yaru
	 */
	public class PlayState extends FlxState 
	{
		//Embeds
		[Embed(source="../assets/data/lvl.tmx", mimeType="application/octet-stream")] private static const Lvl1Tmx:Class;
		[Embed(source = "../assets/images/tiles.png")]  public static const ImgBg1:Class;		
		[Embed(source = "../assets/sounds/Pickup_Coin2.mp3")] private static const S_PICKUP:Class;
			
		//Mapa
		private var hasLoaded:Boolean = false;
		private var background:FlxTilemap;
		private var cave:FlxTilemap;
		private var holes:FlxTilemap;
		private var player:Player;
		private var jewelGroup:FlxGroup;
		private var guiText:FlxText;
		private var darkness:FlxSprite;
		private var light:Light;
		private var itemGroup:FlxGroup;
		private var timer:Number;
				
		override public function create():void 
		{
			guiText = new FlxTextPlus(0, 0, 100);
			guiText.scrollFactor.x = guiText.scrollFactor.y = 0;
			guiText.setFormat(null, 8, Util.YELLOW,"left",Util.BLACK);
			guiText.antialiasing = false;
			light = new Light(0, 0, darkness, Util.WHITE, 1, 1);
			darkness = new FlxSprite(0, 0);
			darkness.makeGraphic(FlxG.width, FlxG.height, Util.BLACK);
			darkness.scrollFactor = new FlxPoint();
			darkness.blend = "multiply";
			jewelGroup = new FlxGroup();
			background = new FlxTilemap();
			itemGroup = new FlxGroup();
			loadStateFromTmx(Lvl1Tmx); // Y esto ya lo carga.
			timer = 0;
			super.create();
		}
		
		private function loadStateFromTmx(TMXFile:Class):void 
		{
			FlxG.flash(0xFFFFFFFF, 1);
			var tmx:TmxMap = new TmxMap(new XML(new TMXFile));
			var csvBg:String = tmx.getLayer('background').toCsv(tmx.getTileSet('tiles'));
			background.loadMap(csvBg, ImgBg1, 16, 16, FlxTilemap.OFF, 0, 0, 2);
			add(background);
			add(light);
			add(jewelGroup);
			add(itemGroup);
			add(darkness);
			
			add(guiText)
			
			light.visible = false;
			darkness.visible = false;
			
			var group:TmxObjectGroup = tmx.getObjectGroup('objects');
			for each (var object:TmxObject in group.objects) 
			{
				spawnObject(object)
			}
			
			FlxG.camera.setBounds(0, 0, background.width + 400, background.height + 300, true);
			FlxG.camera.follow(player, FlxCamera.STYLE_LOCKON);
		}
		
		private function spawnObject(object:TmxObject):void 
		{
			switch (object.type) 
			{
			case "player":
				player = new Player(object.x, object.y);
				add(player);	
				break;
			default:
				jewelGroup.add(new Jewel(object.x, object.y));
			}
					
		}
		
		override public function update():void 
		{
			timer += FlxG.elapsed;
			guiText.text = jewelGroup.countLiving().toString() + " /" + jewelGroup.length.toString() + "\n" + FlxU.formatTime(timer).toString();
			if (player.y > 85) {
				light.visible = darkness.visible = true;
			}
			else {
				light.visible = darkness.visible = false;
			}
			super.update();
			FlxG.collide(player, background);
			FlxG.overlap(player, jewelGroup, playerInJewel);
			
			if (FlxG.keys.justPressed("ENTER")) {
				player.returnToStart();
			}
			
			if (FlxG.keys.justPressed("R")) {
				Util.timer = timer;
				FlxG.switchState(new EndingScreen());
			}
			
			if (jewelGroup.countLiving() == 0) {
				Util.timer = timer;
				FlxG.switchState(new EndingScreen());
			}
						
		}
		
		
		private function playerInJewel(p:Player,j:Jewel):void 
		{
			FlxG.play(S_PICKUP, 0.5, false, false);
			j.kill();
		}
		
		override public function draw():void 
		{
			darkness.fill(Util.BLACK);
			var screenXY:FlxPoint = light.getScreenXY();
			darkness.stamp(light, screenXY.x - light.width / 2, screenXY.y - light.height / 2);
			light.x = player.x;
			light.y = player.y;
			super.draw();
		}
		
	}
}