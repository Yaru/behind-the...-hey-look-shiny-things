package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.system.fscommand;
	import org.flixel.FlxG;
	import org.flixel.FlxGame;
	[SWF(width = "640", height = "480")];
	/**
	 * ...
	 * @author Yaru
	 */
	[Frame(factoryClass="Preloader")]
	public class Main extends FlxGame
	{

		public function Main():void 
		{
			super(320, 240, TitleScreen, 2);
			forceDebugger = true;
		}
		
		override protected function update():void 
		{
			if (FlxG.keys.justPressed("ESCAPE")) {
				fscommand("quit");
			}
			super.update();
		}
	}

}